<?php
include 'conexion.php';
include '../entidades_otablas/usuario.php';

class usuariodao extends Conexion{

	protected static $conexion;

	private static function enviarconexion(){

		//self para invocar la clase

		self::$conexion=Conexion::conectar();

	}


	private static function desconectar(){
		self::$conexion=null;

	}


	//metodo que sirve para validar el login
	public static function login($usuario){

		$query="SELECT * FROM usuarios WHERE username = :usuario and pass= :pass";

		self::enviarconexion();


		$resultado = self::$conexion->prepare($query);
		$resultado->bindValue(":usuario",$usuario->getUsuario());
		$resultado->bindValue(":pass", $usuario->getPassword());
		$resultado->execute();

		//si el resultado tiene datos lo sabremos
		if($resultado->rowCount()>0){

			
			$filas = $resultado->fetch();
			//condicional si filas es igual a usuario y a pass para validar 
			if ($filas["username"] == $usuario->getUsuario() &&
			 $filas ["pass"] == $usuario->getPassword()) {
			 	return true;
				
			}
		}
		return false;

	}



	//metodo que sirve para obtener un usuario
	public static function getUsuario($usuario){

		$query="SELECT id,nombre,pass,email,username,privilegio,fecha_registro FROM usuarios WHERE username = :usuario and pass= :pass";

		self::enviarconexion();


		$resultado = self::$conexion->prepare($query);
		$resultado->bindValue(":usuario",$usuario->getUsuario());
		$resultado->bindValue(":pass", $usuario->getPassword());
		$resultado->execute();
		$filas = $resultado->fetch();


			 	$usuario = new tabla_usuario();
			 	$usuario->setId($filas["id"]);
			 	$usuario->setNombre($filas["nombre"]);
			 	$usuario->setPassword($filas["pass"]);
			 	$usuario->setUsuario($filas["username"]);
			 	$usuario->setEmail($filas["email"]);
			 	$usuario->setPrivilegio($filas["privilegio"]);
			 	$usuario->setFecha_registro($filas["fecha_registro"]);


			 	return $usuario;




 
}

}
?>