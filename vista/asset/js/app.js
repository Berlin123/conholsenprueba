
//
$(document).ready(function(){

	//bind  verifica que despues de hacer lo que se pide ejemplo submit despues de opromi ese boton sse enviara in alert
	$("#loginform").bind("submit", function(){
		//del formulario #login-form estamos obteniendo el metodo en el quese envian los datos
	$.ajax({
			type: $(this).attr("method"),
			url: $(this).attr("action"),
			//campos que se van a recuperar del formulario
			data: $(this).serialize(),
			beforeSend: function(){
				//para cambiar el texto del boton
				$("#loginform button[type=submit]").html("Enviando...");
				//para bloquear el boton
				$("#loginform button[type=submit]").attr("disabled","disabled");

			},

 				//reponse va retornar el estado en el que se encuentra de validar.php
			success: function(reponse){


					//si estado es == true oea ssi hay conexion
				if(reponse.estado == "true"){
				//prueba 1 alert("Coneectado");
				//PRUEBA 2 : ESTO E CONECTA CON OVERHANG.MIN.JS
				$("body").overhang({
  				type: "success",
  				message: "Redirigiendo a Interfaz",
  				//me manda a otro archivo
  				callback:function(){
  				window.location.href= "admin.php";

  					}

				});
				}else{
				$("body").overhang({
  				type: "error",
  				message: "Usario o contrasena incorrecta"
				});
				//para quitar el disabled del boton rapidamentes
				$("#loginform button[type=submit]").removeAttr("disabled");
				}
				

			},
			error: function(){
				$("body").overhang({
  				type: "error",
  				message: "Usario o contrasena incorrecta"
				});

				$("#loginform button[type=submit]").removeAttr("disabled");
			}
		});


		//alert("Enviar formulario")

		return false;

	});


});