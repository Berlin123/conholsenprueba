<?php 
include_once'partes/header.php';

session_start();

if(isset($_SESSION["usuario"])){

	if($_SESSION["usuario"]["privilegio"] == 1){

		header("Location:admin.php");		
	}

}else{

header("Location:login.php");
}

?>

<?php include_once'partes/menu.php'; ?>

<div class="container">

	<div class="starter-template">
		<br><br><br>
		
		<div class="jumbotron">
		<div class="container">
				<h1><strong>Bienvenido</strong> <?php echo $_SESSION["usuario"]["nombre"];  ?></h1>
				<p>Panel de administrador su privilegio es <?php echo $_SESSION["usuario"]["privilegio"]==1?'Admin':'Cliente'; ?></p>
				<p>
					<a href="cerrar.php" class="btn btn-primary btn-lg">Cerrar sesion</a>
				</p>
			</div>
		</div>
	</div>

</div>


<?php 

include_once'partes/footer.php';


?>
